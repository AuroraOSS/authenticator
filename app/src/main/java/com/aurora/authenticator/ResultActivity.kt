package com.aurora.authenticator

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import com.aurora.authenticator.databinding.ActivityResultBinding
import nl.komponents.kovenant.task
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi

class ResultActivity : AppCompatActivity() {

    private lateinit var B: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        B = ActivityResultBinding.inflate(layoutInflater)
        setContentView(B.root)
        onNewIntent(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            val email = intent.getStringExtra(MainActivity.AUTH_EMAIL)
            val token = intent.getStringExtra(MainActivity.AUTH_TOKEN)
            retrieveAc2dmToken(email, token)
        }
    }

    private fun retrieveAc2dmToken(Email: String?, oAuthToken: String?) {
        task {
            AC2DMTask().getAC2DMResponse(Email, oAuthToken)
        } successUi {
            if (it.isNotEmpty()) {
                B.viewFlipper.displayedChild = 1
                B.name.setText(it["firstName"])
                B.email.setText(it["Email"])
                B.auth.setText(it["Auth"])
                B.token.setText(it["Token"])
            } else {
                B.viewFlipper.displayedChild = 2
                Toast.makeText(this, "Failed to generate AC2DM Auth Token", Toast.LENGTH_LONG).show()
            }
        } failUi {
            B.viewFlipper.displayedChild = 2
            Toast.makeText(this, "Failed to generate AC2DM Auth Token", Toast.LENGTH_LONG).show()
        }
    }
}
