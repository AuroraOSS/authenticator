plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.aurora.authenticator"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.aurora.authenticator"
        minSdk = 21
        targetSdk = 34
        versionCode = 3
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }
}

dependencies {
    // Kotlin
    implementation("androidx.core:core-ktx:1.13.1")

    //Google Material
    implementation("com.google.android.material:material:1.12.0")

    //Fuel - HTTP Client
    implementation("com.github.kittinunf.fuel:fuel:2.3.1")

    //Kovenant - For easy async tasks
    implementation("nl.komponents.kovenant:kovenant:3.3.0")
    implementation("nl.komponents.kovenant:kovenant-android:3.3.0")
}
